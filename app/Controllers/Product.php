<?php

namespace App\Controllers;

class Product extends BaseController
{
	public function __construct()
	{
		helper('form');
		$this->validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		$productModel = new \App\Models\Product();
		$products = $productModel->findAll();

		return view('product/index', [
			'products' => $products,
		]);
	}

	public function view()
	{
		$id = $this->request->uri->getSegment(3);
		$productModel = new \App\Models\Product();
		$product = $productModel->find($id);

		return view('product/view', [
			'product' => $product,
		]);
	}

	public function create()
	{
		if ($this->request->getPost()) {
			$data = $this->request->getPost();
			$this->validation->run($data, 'product');
			$errors = $this->validation->getErrors();

			if (!$errors) {
				$productModel = new \App\Models\Product();
				$product = new \App\Entities\Product();

				$product->fill($data);
				$product->image = $this->request->getFile('image');
				$product->created_by = $this->session->get('id');
				$product->created_at = date("Y-m-d H:i:s");
				$productModel->save($product);
				$id = $productModel->insertID();

				$segments = ['product', 'view', $id];
				return redirect()->to(site_url($segments));
			}
		}
		return view('product/create');
	}

	public function update()
	{
		$id = $this->request->uri->getSegment(3);
		$productModel = new \App\Models\Product();
		$product = $productModel->find($id);

		if ($this->request->getPost()) {
			$data = $this->request->getPost();
			$this->validation->run($data, 'productupdate');
			$errors = $this->validation->getErrors();

			if (!$errors) {
				$b = new \App\Entities\Product();
				$b->id = $id;
				$b->fill($data);

				if ($this->request->getFile('image')->isValid()) {
					$b->image = $this->request->getFile('image');
				}

				$b->updated_by = $this->session->get('id');
				$b->updated_at = date("Y-m-d H:i:s");
				$productModel->save($b);

				$segments = ['Product', 'view', $id];

				return redirect()->to(base_url($segments));
			}
		}

		return view('product/update', [
			'product' => $product,
		]);
	}

	public function delete()
	{
		$id = $this->request->uri->getSegment(3);

		$modelProduct = new \App\Models\Product();
		$delete = $modelProduct->delete($id);

		return redirect()->to(site_url('product/index'));
	}
}
