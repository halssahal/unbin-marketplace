<?php

namespace App\Controllers;

class Catalog extends BaseController
{
	private $url = "https://api.rajaongkir.com/starter/";
	private $apiKey = "ae4b0421f38dd6cd9ae8bc74a55b76e1";

	public function __construct()
	{
		helper('form');
		$this->validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		$product = new \App\Models\Product();
		$products = $product->findAll();
		return view('catalog/index', [
			'products' => $products,
		]);
	}

	public function order()
	{
		$id = $this->request->uri->getSegment(3);

		$modelProduct = new \App\Models\Product();
		$product = $modelProduct->find($id);

		$province = $this->rajaongkir('province');

		if ($this->request->getPost()) {
			$data = $this->request->getPost();
			$this->validation->run($data, 'order');
			$errors = $this->validation->getErrors();

			if (!$errors) {
				$orderModel = new \App\Models\Order();
				$order = new \App\Entities\Order();

				$productModel = new \App\Models\Product();
				$product_id = $this->request->getPost('product_id');
				$qty = $this->request->getPost('jumlah');

				$product = $productModel->find($product_id);
				$entityProduct = new \App\Entities\Product();
				$entityProduct->id = $product_id;

				$entityProduct->stock = $product->stock - $qty;
				$productModel->save($entityProduct);

				$order->fill($data);
				$order->status = 0;
				$order->created_by = $this->session->get('id');
				$order->created_at = date("Y-m-d H:i:s");

				$orderModel->save($order);
				$id = $orderModel->insertID();

				$segment = ['order', 'view', $id];
				return redirect()->to(site_url($segment));
			}
		}

		return view('catalog/order', [
			'product' => $product,
			'province' => json_decode($province)->rajaongkir->results,
		]);
	}

	public function getCity()
	{
		if ($this->request->isAJAX()) {
			$province_id = $this->request->getGet('province_id');
			$data = $this->rajaongkir('city', $province_id);
			return $this->response->setJSON($data);
		}
	}

	public function getCost()
	{
		if ($this->request->isAJAX()) {
			$origin = $this->request->getGet('origin');
			$destination = $this->request->getGet('destination');
			$weight = $this->request->getGet('weight');
			$courier = $this->request->getGet('courier');
			$data = $this->rajaongkircost($origin, $destination, $weight, $courier);
			return $this->response->setJSON($data);
		}
	}

	private function rajaongkircost($origin, $destination, $weight, $courier)
	{

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=" . $origin . "&destination=" . $destination . "&weight=" . $weight . "&courier=" . $courier,
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: " . $this->apiKey,
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return $response;
	}


	private function rajaongkir($method, $id_province = null)
	{
		$endPoint = $this->url . $method;

		if ($id_province != null) {
			$endPoint = $endPoint . "?province=" . $id_province;
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $endPoint,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: " . $this->apiKey
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return $response;
	}
}
