<?php

namespace App\Controllers;

use TCPDF;

class Order extends BaseController
{
	public function __construct()
	{
		helper('form');
		$this->validation = \Config\Services::validation();
		$this->session = session();
		$this->email = \Config\Services::email();
	}

	public function view()
	{
		$id = $this->request->uri->getSegment(3);

		$orderModel = new \App\Models\Order();
		$order = $orderModel
			->select('*, orders.id AS id_trans, products.name as product_name, users.username as user_name')
			->join('products', 'products.id=orders.product_id')
			->join('users', 'users.id=orders.user_id')
			->where('orders.id', $id)
			->first();

		return view('order/view', [
			'order' => $order,
		]);
	}

	public function index()
	{
		$orderModel = new \App\Models\Order();
		$orders = $orderModel
			->select('*, orders.id AS order_id, products.name as product_name, users.username as user_name')
			->join('products', 'products.id=orders.product_id')
			->join('users', 'users.id=orders.user_id')
			->findAll();

		return view('order/index', [
			'orders' => $orders,
		]);
	}

	public function invoice()
	{
		$id = $this->request->uri->getSegment(3);

		$orderModel = new \App\Models\Order();
		$order = $orderModel->find($id);

		$userModel = new \App\Models\User();
		$user = $userModel->find($order->user_id);

		$productModel = new \App\Models\Product();
		$product = $productModel->find($order->product_id);

		$html = view('order/invoice', [
			'order' => $order,
			'user' => $user,
			'product' => $product,
		]);

		$pdf = new TCPDF('L', PDF_UNIT, 'A5', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('John Doe');
		$pdf->SetTitle('Invoice');
		$pdf->SetSubject('Invoice');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->addPage();

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');
		//line ini penting
		$this->response->setContentType('application/pdf');
		//Close and output PDF document
		$pdf->Output(FCPATH . '/uploads/invoice.pdf', 'I');
	}

	private function sendEmail($attachment, $to, $title, $message)
	{
		$this->email->setFrom('deavenditama@gmail.com', 'deavenditama');
		$this->email->setTo($to);

		$this->email->attach($attachment);

		$this->email->setSubject($title);

		$this->email->setMessage($message);

		if (!$this->email->send()) {
			return false;
		} else {
			return true;
		}
	}
}
