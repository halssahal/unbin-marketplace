<?php

namespace App\Database\Seeds;

class UserSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $salt = uniqid('', true);
        $password = md5($salt . '123456');
        $data = [
            'username' => 'admin',
            'password' => $password,
            'salt' => $salt,
            'avatar' => NULL,
            'role' => 0, //admin
            'created_by' => 0,
            'created_at' => date("Y-m-d H:i:s"),
        ];

        $this->db->table('users')->insert($data);
    }
}
