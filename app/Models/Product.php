<?php

namespace App\Models;

use CodeIgniter\Model;

class Product extends Model
{
	protected $table = 'products';
	protected $primaryKey = 'id';
	protected $allowedFields = [
		'id',
		'name',
		'price',
		'stock',
		'image',
		'created_at',
		'updated_at',
		'created_by',
		'updated_by'
	];
	protected $returnType = 'App\Entities\Product';
	protected $useTimestamps = false;
}
