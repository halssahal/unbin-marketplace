<?php namespace App\Models;

use CodeIgniter\Model;

class Order extends Model
{
	protected $table = 'orders';
	protected $primaryKey = 'id';
	protected $allowedFields = [
		'id',
		'product_id',
		'user_id',
		'qty',
		'total_price',
		'address',
		'shipping',
		'status',
		'created_at',
		'updated_at',
		'created_by',
		'updated_by'
	];
	protected $returnType = 'App\Entities\Order';
	protected $useTimestamps = false;
}