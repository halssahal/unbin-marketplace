<?php

namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{
	protected $table = 'users';
	protected $primaryKey = 'id';
	protected $allowedFields = [
		'id',
		'username',
		'password',
		'salt',
		'avatar',
		'role',
		'created_at',
		'updated_at',
		'created_by',
		'updated_by'
	];
	protected $returnType = 'App\Entities\User';
	protected $useTimestamps = false;
}
