<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Welcome</title>

  <!-- Bootstrap core CSS -->
  <link href="<?= base_url('bootstrap-4.0.0/css/bootstrap.min.css') ?>" rel="stylesheet">

  <!-- Custom styles for this template -->
  <style>
    body {
      padding-top: 5rem;
    }

    .starter-template {
      padding: 3rem 1.5rem;
      text-align: center;
    }
  </style>
</head>

<body>

  <?= $this->include('navbar') ?>

  <main role="main" class="container">

    <?= $this->renderSection('content') ?>

  </main><!-- /.container -->
  <script src="<?= base_url('jquery-3.5.1.min.js') ?>"></script>
  <script src="<?= base_url('bootstrap-4.0.0/js/bootstrap.min.js') ?>"></script>
  <?= $this->renderSection('script') ?>
</body>

</html>