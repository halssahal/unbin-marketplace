<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<h1>Barang</h1>
<table class="table">
	<thead>
		<th>No</th>
		<th>Barang</th>
		<th>Gambar</th>
		<th>Harga</th>
		<th>Stok</th>
		<th>Aksi</th>
	</thead>
	<tbody>
		<?php foreach ($products as $index => $product) : ?>
			<tr>
				<td><?= ($index + 1) ?></td>
				<td><?= $product->name ?></td>
				<td>
					<img class="img-fluid" width="200px" alt="gambar" src="<?= base_url('uploads/' . $product->image) ?>" />
				</td>
				<td><?= $product->price ?></td>
				<td><?= $product->stock ?></td>
				<td>
					<a href="<?= site_url('product/view/' . $product->id) ?>" class="btn btn-primary">View</a>
					<a href="<?= site_url('product/update/' . $product->id) ?>" class="btn btn-success">Update</a>
					<a href="<?= site_url('product/delete/' . $product->id) ?>" class="btn btn-danger">Delete</a>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
<?= $this->endSection() ?>