<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<h1>View Barang</h1>
<div class="container">
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<img class="img-fluid" alt="image" src="<?= base_url('uploads/' . $product->image) ?>" />
				</div>
			</div>
		</div>
		<div class="col-6">
			<h1 class="text-success"><?= $product->name ?></h1>
			<h4>Harga : <?= $product->price ?></h4>
			<h4>Stok : <?= $product->stock ?></h4>
		</div>
	</div>
</div>
<?= $this->endSection() ?>