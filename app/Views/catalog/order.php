<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<?php
$product_id = [
	'name' => 'product_id',
	'id' => 'product_id',
	'value' => $product->id,
	'type' => 'hidden'
];

$user_id = [
	'name' => 'user_id',
	'id' => 'user_id',
	'value' => session()->get('id'),
	'type' => 'hidden'
];
$qty = [
	'name' => 'qty',
	'id' => 'qty',
	'value' => 1,
	'min' => 1,
	'class' => 'form-control',
	'type' => 'number',
	'max' => $product->stock,
];
$total_price = [
	'name' => 'total_price',
	'id' => 'total_price',
	'value' => null,
	'class' => 'form-control',
	'readonly' => true,
];
$shipping = [
	'name' => 'shipping',
	'id' => 'shipping',
	'value' => null,
	'class' => 'form-control',
	'readonly' => true,
];
$address = [
	'name' => 'address',
	'id' => 'address',
	'class' => 'form-control',
	'value' => null,
];

$submit = [
	'name' => 'submit',
	'id' => 'submit',
	'type' => 'submit',
	'value' => 'Beli',
	'class' => 'btn btn-success',
];
?>

<div class="container">
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<img class="img-fluid" src="<?= base_url('uploads/' . $product->image) ?>" />
					<h1 class="text-success"><?= $product->name ?></h1>
					<h4> Harga : <?= $product->price ?></h4>
					<h4> Stok : <?= $product->stock ?></h4>
				</div>
			</div>
		</div>
		<div class="col-6">
			<h4>Pengiriman</h4>
			<div class="form-group">
				<label for="province">Pilih Provinsi</label>
				<select class="form-control" id="province">
					<option>Select Provinsi</option>
					<?php foreach ($province as $p) : ?>
						<option value="<?= $p->province_id ?>"><?= $p->province ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="form-group">
				<label for="kabupaten">Pilih Kabupaten/Kota</label>
				<select class="form-control" id="kabupaten">
					<option>Select Kabupaten/kota</option>
				</select>
			</div>
			<div class="form-group">
				<label for="service">Pilih Service</label>
				<select class="form-control" id="service">
					<option>Select Service</option>
				</select>
			</div>

			<strong>Estimasi : <span id="estimasi"></span></strong>
			<hr>
			<?= form_open('Catalog/order') ?>
			<?= form_input($product_id) ?>
			<?= form_input($user_id) ?>
			<div class="form-group">
				<?= form_label('Jumlah Pembelian', 'qty') ?>
				<?= form_input($qty) ?>
			</div>
			<div class="form-group">
				<?= form_label('Ongkir', 'shipping') ?>
				<?= form_input($shipping) ?>
			</div>
			<div class="form-group">
				<?= form_label('Total Harga', 'total_price') ?>
				<?= form_input($total_price) ?>
			</div>
			<div class="form-group">
				<?= form_label('Alamat', 'address') ?>
				<?= form_input($address) ?>
			</div>
			<div class="text-right">
				<?= form_submit($submit) ?>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>

<?= $this->endSection() ?>
<?= $this->section('script') ?>
<script>
	$('document').ready(function() {
		var qty = 1;
		var price = <?= $product->price ?>;
		var shipping = 0;
		$("#province").on('change', function() {
			$("#kabupaten").empty();
			var id_province = $(this).val();
			$.ajax({
				url: "<?= site_url('catalog/getcity') ?>",
				type: 'GET',
				data: {
					'id_province': id_province,
				},
				dataType: 'json',
				success: function(data) {
					console.log(data);
					var results = data["rajaongkir"]["results"];
					for (var i = 0; i < results.length; i++) {
						$("#kabupaten").append($('<option>', {
							value: results[i]["city_id"],
							text: results[i]['city_name']
						}));
					}
				},

			});
		});

		$("#kabupaten").on('change', function() {
			var id_city = $(this).val();
			$.ajax({
				url: "<?= site_url('catalog/getcost') ?>",
				type: 'GET',
				data: {
					'origin': 154,
					'destination': id_city,
					'weight': 1000,
					'courier': 'jne'
				},
				dataType: 'json',
				success: function(data) {
					console.log(data);
					var results = data["rajaongkir"]["results"][0]["costs"];
					for (var i = 0; i < results.length; i++) {
						var text = results[i]["description"] + "(" + results[i]["service"] + ")";
						$("#service").append($('<option>', {
							value: results[i]["cost"][0]["value"],
							text: text,
							etd: results[i]["cost"][0]["etd"]
						}));
					}
				},

			});
		});

		$("#service").on('change', function() {
			var estimasi = $('option:selected', this).attr('etd');
			shipping = parseInt($(this).val());
			$("#shipping").val(shipping);
			$("#estimasi").html(estimasi + " Hari");
			var total_price = (qty * price) + shipping;
			$("#total_price").val(total_price);
		});

		$("#qty").on("change", function() {
			qty = $("#qty").val();
			var total_price = (qty * price) + shipping;
			$("#total_price").val(total_price);
		});
	});
</script>
<?= $this->endSection() ?>