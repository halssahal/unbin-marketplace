<?= $this->extend('layout') ?>
<?= $this->section('content') ?>

<div class="container">
	<div class="row">
		<?php foreach ($products as $product) : ?>
			<div class="col-4">
				<div class="card text-center">
					<div class="card-header">
						<span class="text-success"><strong><?= $product->name ?></strong></span>
					</div>
					<div class="card-body">
						<img class="img-thumbnail" style="max-height: 200px" src="<?= base_url('uploads/' . $product->image) ?>" />
						<h5 class="mt-3 text-success"><?= "Rp " . number_format($product->price, 2, ',', '.') ?></h5>
						<p class="text-info">Stok : <?= $product->stock ?></p>
					</div>
					<div class="card-footer">
						<a href="<?= site_url('catalog/order/' . $product->id) ?>" style="width:100%" class="btn btn-success">Beli</a>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>

<?= $this->endSection() ?>