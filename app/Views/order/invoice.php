<html>

<head>
	<style>
		table {
			border-collapse: collapse;
			width: 100%;
		}

		td,
		th {
			border: 1px solid #000000;
			text-align: center;
		}
	</style>
</head>

<body>
	<div style="font-size:24px; color:'#dddddd' "><i>Invoice</i></div>
	<p>
		<i>Barang Bekas Shop</i><br>
		Bogor, Indonesia<br>
		628xxxxxx
	</p>
	<hr>
	<hr>
	<p></p>
	<p>
		Pembeli : <?= $user->username ?><br>
		Alamat : <?= $order->address ?><br>
		Transaksi No : <?= $order->id ?><br>
		Tanggal : <?= date('Y-m-d', strtotime($order->created_at)) ?>
	</p>
	<table cellpadding="6">
		<tr>
			<th><strong>Barang</strong></th>
			<th><strong>Harga Satuan</strong></th>
			<th><strong>Jumlah</strong></th>
			<th><strong>Ongkir</strong></th>
			<th><strong>Total Harga</strong></th>
		</tr>
		<tr>
			<td><?= $product->name ?></td>
			<td><?= "Rp " . number_format($product->price, 2, ',', '.') ?></td>
			<td><?= $order->qty ?></td>
			<td><?= "Rp " . number_format($order->shipping, 2, ',', '.') ?></td>
			<td><?= "Rp " . number_format($order->total_price, 2, ',', '.') ?></td>
		</tr>
	</table>
</body>

</html>