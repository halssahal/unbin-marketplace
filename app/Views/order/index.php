<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<h1>Transaksi</h1>
<table class="table">
	<thead>
		<tr>
			<th>No</th>
			<th>Barang</th>
			<th>Pembeli</th>
			<th>Alamat</th>
			<th>Jumlah</th>
			<th>Harga</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($orders as $index => $order) : ?>
			<tr>
				<td><?= $order->order_id ?></td>
				<td><?= $order->product_name ?></td>
				<td><?= $order->user_name ?></td>
				<td><?= $order->address ?></td>
				<td><?= $order->qty ?></td>
				<td><?= $order->total_price ?></td>
				<td>
					<a href="<?= site_url('order/view/' . $order->order_id) ?>" class="btn btn-primary">View</a>
					<a href="<?= site_url('order/invoice/' . $order->order_id) ?>" class="btn btn-info" target="_blank">Invoice</a>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
<?= $this->endSection() ?>