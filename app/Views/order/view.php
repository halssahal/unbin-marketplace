<?= $this->extend('layout') ?>
<?= $this->section('content') ?>
<h4>Transaksi No <?= $order->id ?></h4>
<table class="table">
	<tr>
		<td>Barang</td>
		<td><?= $order->product_name ?></td>
	</tr>
	<tr>
		<td>Pembeli</td>
		<td><?= $order->user_name ?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td><?= $order->address ?></td>
	</tr>
	<tr>
		<td>Jumlah</td>
		<td><?= $order->qty ?></td>
	</tr>
	<tr>
		<td>Total Harga</td>
		<td><?= $order->total_price ?></td>
	</tr>
</table>
<?= $this->endSection() ?>